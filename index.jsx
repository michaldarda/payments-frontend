import React from 'react';

window.$ = require('jquery');
window.Backbone = require('backbone');
window._ = require('underscore');

const BACKEND_URL = process.env.BACKEND_URL || 'http://localhost:4000/';
const AUTH_SERVICE_URL = process.env.AUTH_SERVICE_URL || 'http://localhost:4000/';
const ACCOUNT_SERVICE_URL = process.env.ACCOUNT_SERVICE_URL || 'http://localhost:4000/';
const OPERATION_SERVICE_URL = process.env.OPERATION_SERVICE_URL || 'http://localhost:4000/';

class RegistrationForm extends React.Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(e) {
    e.preventDefault();

    let login = React.findDOMNode(this.refs.login).value.trim();
    let email = React.findDOMNode(this.refs.email).value.trim();
    let password = React.findDOMNode(this.refs.password).value.trim();
    let password_confirmation = React.findDOMNode(this.refs.password_confirmation).value.trim();

    let requestObject = {
      "user": {
        "login":login,
        "email":email,
        "password":password,
        "password_confirmation":password_confirmation
      }
    };

    $.ajax(AUTH_SERVICE_URL + "/registration", {
      data : JSON.stringify(requestObject),
      contentType : 'application/json',
      type : 'POST',
      success: function(data) {
        localStorage.setItem('token', data.token);

        Backbone.history.navigate('', true);
      }
    });

    React.findDOMNode(this.refs.login).value = '';
    React.findDOMNode(this.refs.email).value = '';
    React.findDOMNode(this.refs.password).value = '';
    React.findDOMNode(this.refs.password_confirmation).value = '';

    return;
  }
  render() {
    return (
      <form className="vertical" onSubmit={this.handleSubmit}>
        <input ref="login" type="text" placeholder="login" />
        <input ref="email" type="email" placeholder="email" />
        <input ref="password" type="password" placeholder="password" />
        <input ref="password_confirmation" type="password" placeholder="password confirmation" />

        <button type="submit">Login</button>
      </form>);
  }
};

class AccountBalance extends React.Component {
  constructor(props) {
    super(props);

    this.state = {balance: 0.0};

    this.updateBalance = this.updateBalance.bind(this);

    $.ajax(ACCOUNT_SERVICE_URL + "/account_balance", {
      contentType : 'application/json',
      headers: {
        "Authorization": ("Bearer " + localStorage.getItem("token"))
      },
      type : 'GET',
      success: this.updateBalance
    });
  }

  updateBalance(data) {
    this.setState({balance: data.data.balance});
  }

  render() {
    return(<h5>Account balance {this.state.balance} N</h5>);
  }
};

class AccountHistoryEntry extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <tr className="first">
        <td>{this.props.entry.created_at}</td>
        <td>{this.props.entry.description}</td>
      </tr>
    );
  }
};

class AccountHistory extends React.Component {
  constructor(props) {
    super(props);

    this.state = { entries: [] };

    this.updateEntries = this.updateEntries.bind(this);

    $.ajax(BACKEND_URL + "/account_actions", {
      contentType : 'application/json',
      headers: {
        "Authorization": ("Bearer " + localStorage.getItem("token"))
      },
      type : 'GET',
      success: this.updateEntries
    });
  }
  updateEntries(data) {
    this.setState({entries: data.data});
  }
  render() {
    return (
      <table className="striped tight sortable" cellspacing="0" cellpadding="0">
        <thead>
          <tr className="alt first last">
            <th className="datetd center">Date</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {this.state.entries.map(function(entry) {
            return (<AccountHistoryEntry key={entry.id} entry={entry} />);
          })}
        </tbody>
      </table>);
  }
};

class Account extends React.Component {
  render() {
    return (
      <div>
        <AccountBalance />

        <AccountHistory />
      </div>)
  }
}

class Transfer extends React.Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(e) {
    e.preventDefault();

    let email = React.findDOMNode(this.refs.email).value.trim();
    let amount = React.findDOMNode(this.refs.amount).value.trim();

    let requestObject = {
      "transfer": {
        "amount":parseFloat(amount),
        "email":email,
      }
    };

    $.ajax(OPERATION_SERVICE_URL + "/deposit", {
      data : JSON.stringify(requestObject),
      contentType : 'application/json',
      headers: {
        "Authorization": ("Bearer " + localStorage.getItem("token"))
      },
      type : 'POST',
      success: function(data) {
        Backbone.history.navigate("account", true);
      }
    });

    React.findDOMNode(this.refs.email).value = '';
    React.findDOMNode(this.refs.amount).value = '';

    return;
  }
  render() {
    return (
      <form className="vertical" onSubmit={this.handleSubmit}>
        <input ref="email" type="email" placeholder="email" />
        <input ref="amount" type="text" placeholder="amount" />

        <button type="submit">Send</button>
      </form>);
  }
}


class LoginForm extends React.Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(e) {
    e.preventDefault();

    let login = React.findDOMNode(this.refs.login).value.trim();
    let password = React.findDOMNode(this.refs.password).value.trim();

    let requestObject = {
      "user": {
        "login":login,
        "password":password,
      }
    };

    $.ajax(AUTH_SERVICE_URL + "/sessions", {
      data : JSON.stringify(requestObject),
      contentType : 'application/json',
      type : 'POST',
      success: function(data) {
        console.log(data);

        localStorage.setItem('token', data.token);

        Backbone.history.navigate('', true);
      }
    });

    React.findDOMNode(this.refs.login).value = '';
    React.findDOMNode(this.refs.password).value = '';

    return;
  }
  render() {
    return (
      <form className="vertical" onSubmit={this.handleSubmit}>
        <input ref="login" type="text" placeholder="login" />
        <input ref="password" type="password" placeholder="password" />

        <button type="submit">Login</button>
      </form>);
  }
}

class UserNavigation extends React.Component {
  constructor() {
    super();
    this.handleLogout = this.handleLogout.bind(this);
  }
  handleLogout(e) {
    e.preventDefault();

    localStorage.removeItem("token");

    Backbone.history.navigate("login", true);
  }
  render() {
    return (
      <ul className="menu">
        <li><a href="#account">Account</a></li>
        <li><a href="#transfer">Transfer</a></li>
        <li><a onClick={this.handleLogout}>Logout</a></li>
      </ul>
    );
  }
};

class GuestNavigation extends React.Component {
  render() {
    return (
      <ul className="menu">
        <li><a href="#login">Login</a></li>
        <li><a href="#register">Register</a></li>
      </ul>
    );
  }
};

function Page(Navbar, Content) {
  return (class extends React.Component {
    render() {
      return (
        <div>
          <Navbar />
          <div className="col_4"></div>
          <div className="col_4 center flex">
            <Content />
          </div>
        </div>);
    }
  });
};

function withAuthorization(router, f) {
  let tokenExists = localStorage.getItem('token');

  if (tokenExists) {
    f();
  } else {
    router.navigate("/login", true)
  }
};

function allowingOnlyGuests(router, f) {
  let tokenExists = localStorage.getItem('token');

  if (tokenExists) {
    router.navigate("/", true)
  } else {
    f();
  }
};

var Router = Backbone.Router.extend({
  routes : {
    "": "account",
    "account": "account",
    "login": "login",
    "register": "register",
    "transfer": "transfer"
  },
  login: function() {
    allowingOnlyGuests(this, function() {
      let LoginPage = Page(GuestNavigation, LoginForm);

      React.render(
        <LoginPage />,
        document.getElementById('container'));
    });
  },
  register: function() {
    allowingOnlyGuests(this, function() {
      let RegistrationPage = Page(GuestNavigation, RegistrationForm);

      React.render(
        <RegistrationPage />,
        document.getElementById('container'));
    });
  },
  account: function() {
    withAuthorization(this, function() {
      let AccountPage = Page(UserNavigation, Account);

      React.render(
        <AccountPage />,
        document.getElementById('container'));
    });
  },
  transfer: function() {
    withAuthorization(this, function() {
      let TransferPage = Page(UserNavigation, Transfer);

      React.render(
        <TransferPage />,
        document.getElementById('container'));
    });
  }
});

new Router();

Backbone.history.start();
