#!/bin/bash

docker pull michaldarda/payments-frontend

docker rm -f payments_frontend

docker run --name payments_frontend \
  -p 80:80 \
  -p 443:443 \
  -e BACKEND_URL=http://192.168.0.201 \
  -e AUTH_SERVICE_URL=http://192.168.0.201:81 \
  -e ACCOUNT_SERVICE_URL=http://192.168.0.201:82 \
  -e OPERATION_SERVICE_URL=http://192.168.0.201:83 \
  -d michaldarda/payments-frontend
